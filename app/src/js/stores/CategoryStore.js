import {EventEmitter} from 'events';
import Category from '../components/Bookmark.js';

class CategoryStore extends EventEmitter {
  constructor() {
    super();
    this.state = {
      items: [
        new this.Category(1, "Informatique".toLowerCase(),"#8aa434"),
        new this.Category(2, "Cuisine".toLowerCase(), "#4f4487"),
        new this.Category(3, "Sport".toLowerCase(), "#cd5536")
      ]};
  }

  Category(id, title, color) {
    this.id = id;
    this.title = title;
    this.color = color;
  }

  create(id, title, color) {
    this.state.items.push({
      id: id,
      title: title,
      color: color
    });

    this.emit('change');
  }

  getAll() {
    return this.state.items;
  }
}

const store = new CategoryStore;

export default store;
