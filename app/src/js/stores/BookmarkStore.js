import {EventEmitter} from 'events';
import img from '../../images/image.jpg';
import Bookmark from '../components/Bookmark.js';

class BookmarkStore extends EventEmitter {
  constructor() {
    super();
    this.state = {
      items: [
        new this.Bookmark(1, "Book1", "http://toto.com", "Informatique", img),
        new this.Bookmark(2, "Book2", "http://toto2.com", "Cuisine", img)
      ]};

      //var fd = fs.openSync("/database/bookmark.json", 'w');

      // TODO: console.log of img to put in json file
      /*var getHttp = new XMLHttpRequest();
      // TODO: Create file if not exists
      getHttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          try {
            var bookmarks = JSON.parse(this.responseText);
            // TODO: use this array for displaying
            console.log(bookmarks);
          }
          catch(e) {
            console.log("File not exists or file is not a json file or file is not a valid json array.");
            // Generate file
            var createHttp = new XMLHttpRequest();
            createHttp.open("POST", "database/bookmarks.json", true);
            createHttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
            console.log(createHttp);
            createHttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                console.log(createHttp.responseText);
              }
              else {
                console.log(createHttp.responseText);
              }
            };
            createHttp.send("[]");
          }
        }
      };
      getHttp.open("GET", "database/bookmarks.json", true);
      getHttp.send();*/
  }

  Bookmark(id, name, url, category, image) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.category = category;
    this.image = image;
  }

  create(bookmark) {
    this.state.items.push(bookmark);
    this.emit('change');
  }

  getAll() {
    return this.state.items;
  }
}

const store = new BookmarkStore;

export default store;
