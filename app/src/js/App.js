import React, { Component } from 'react';
import '../css/App.css';
import Toolbar from './components/Toolbar.js';
import Tabs from './components/Tabs.js';
import Tab from './components/Tab.js';
import PageFragment from './components/PageFragment.js';
import List from './components/List.js';
import Bookmark from './components/Bookmark.js';
import Category from './components/Category.js';
import BookmarkForm from './components/BookmarkForm.js';
import Dialog from './components/Dialog.js';
import AppContextProvider from './components/AppContextProvider.js';
import menuIcon from '../icons/menu.svg';
import closeIcon from '../icons/cross.svg';
import logo from '../icons/logo.svg';
import settingsIcon from '../icons/settings.svg';
import searchIcon from '../icons/search.svg';
import BookmarkStore from './stores/BookmarkStore.js';

export const AppContext = React.createContext();

class App extends Component {

  constructor() {
    super();
    this.state = {
      tab: 'Bookmarks',
      bookmarks: [{binder: this.bindBookmark, items: BookmarkStore.getAll()}],
      dialog: ''
    };

    this.onClickMainAction = this.onClickMainAction.bind(this);
    this.showDialog = this.showDialog.bind(this);
  }

  componentWillMount() {
    BookmarkStore.on('change', () => {
      this.setState({
        bookmarks: [{binder: this.bindBookmark, list: BookmarkStore.getAll()}]
      });
    });
  }

  showDialog(dialog) {
    this.setState({dialog});
  }

  onClickMainAction() {
    this.showDialog(
      <Dialog closeIcon={closeIcon} title="New bookmark">
        <BookmarkForm submitText="Add" />
      </Dialog>
    );
  }

  bindCategory(item) {
    return(
      <Category name={item.name} />
    );
  }

  bindBookmark(item) {
    return (
      <Bookmark
        key={item.id}
        name={item.name}
        url={item.url}
        category={item.category}
        image={item.image}
        menuIcon={menuIcon} />
    );
  }

  render() {
    var tab = this.state.tab;
    var dialog = this.state.dialog;

    /*if (tab != 'Bookmarks') {
    this.setState({
    itemType: Category
    })
    }*/

    return (
      <AppContextProvider showDialog={this.showDialog}>
        <div className="App">
          <Toolbar title="Bookmarks"
            mainActionText="New bookmark"
            mainAction={this.onClickMainAction}
            logo={logo}
            settingsIcon={settingsIcon}
            searchIcon={searchIcon} />
          <Tabs>
            <div className="in">
              <Tab title="Bookmarks" />
              <Tab title="Categories" />
            </div>
            <div className="in tc">
              <Tab title="Bookmarks" />
              <Tab title="Categories" />
            </div>
          </Tabs>
          <PageFragment title={tab}>
            {dialog}
            <List lists={this.state.bookmarks}/>
          </PageFragment>
        </div>
      </AppContextProvider>
    );
  }
}

export default App;
