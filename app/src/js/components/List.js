import React, { Component } from 'react';

class List extends Component {
  constructor() {
    super();
    this.renderItems = this.renderItems.bind(this);
  }

  /*addItem(item, type) {
  // ListItem is an object with a type and that contain an item object of this type
  const listItem = {
  item: this.props.item,
  type: type
  };
  this.setState({
  items: [...this.props.items, listItem]
  });
  }*/

  /*removeItem(item) {
  const list = this.props.items;
  const pos = list.indexOf(item);
  list.splice(pos, 1);
  this.setState({
  items: list
  });
  }*/

  renderItems() {
    // For each type of element
     return this.props.lists.map((type) => {
        // For each item of list
        return type.items.map((item) => {
           return type.binder(item);
        });
      });
  }

  render() {
    return (
      <div className="App-list">
        {this.renderItems()}
      </div>
    );
  }
}

export default List;
