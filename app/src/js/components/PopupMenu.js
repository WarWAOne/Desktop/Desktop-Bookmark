import React, { Component } from 'react';

class PopupMenu extends Component {
  render() {
    console.log(this.props.shown)
    var showClass = this.props.shown ? 'show' : '';

    return (
      <ul className={'App-popupmenu card '+ showClass}>
        {this.props.children}
      </ul>
    );
  }
}

export default PopupMenu;
