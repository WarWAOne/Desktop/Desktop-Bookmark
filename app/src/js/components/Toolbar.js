import React, { Component } from 'react';

import Search from './Search.js';

class Toolbar extends Component {
  render() {
    return (
      <header className="App-toolbar">
        <img src={this.props.logo} className="App-logo" />
        <Search icon={this.props.searchIcon} />
        <h1 className="App-title">{this.props.title}</h1>
        <button className="App-button-main card"
          type="button" onClick={this.props.mainAction.bind(this)}>
          {this.props.mainActionText}
        </button>
        <img src={this.props.settingsIcon} className="App-settings" />
      </header>
    );
  }
}

export default Toolbar;
