import React, { Component } from 'react';

class Dialog extends Component {

  constructor() {
    super();
    this.state = {
      shown: true
    }
  }

  close() {
    this.setState({
      shown: false
    })
  }

  render() {
    if (this.state.shown) {
      return (
            <dialog className="card App-dialog">
              <img src={this.props.closeIcon} onClick={this.close.bind(this)} />
              <h1>{this.props.title}</h1>
              {this.props.children}
            </dialog>
        );
      }
      else {
        this.state = {
          shown: true
        }
        return('')
      }

    }
  }

  export default Dialog;
