package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"warwaone.com/Framework/JsonDatabase"
)

const DATABASE_FOLDER = "../database"
const TABLE_BOOKMARK = "bookmark"

func main() {
	router := chi.NewRouter()

	db := JsonDatabase.Database{Path: DATABASE_FOLDER}

	// TODO: Same port as the app /api/<token> for the api

	router.Get("/", func(response http.ResponseWriter, req *http.Request) {
		response.Write([]byte("OK"))
	})

	// TODO: Régler propb import et propriétées héritées
	dbBookmarkTable := db.GetTable(TABLE_BOOKMARK)
	bookmarkTable := BookmarkTable{dbBookmarkTable}

	// Route Bookmarks
	router.Route("/bookmarks", func(router chi.Router) {
		router.Get("/", bookmarkTable.GetAll)
		//router.Post("/", bookmarkTable.create(bookmark))
		router.Route("/{id}", func(router chi.Router) {
			router.Get("/", bookmarkTable.Get)
			//touter.Put("/", )
			//router.Delete("/", )
		})
	})

	//db.GetTable("bookmark").
	/*Route Users
	router.Route("/users", func(router chi.Router) {
		//router.Get("/", table.GetAllUsers)
		//router.Post("/", createUser)

		// One users
		router.Route("/{id}", func(router chi.Router) {
			//router.Get("/", getUser)
			//router.Put("/", editUser)
			//router.Delete("/", deleteUser)
		})
	})*/

	http.ListenAndServe(":3333", router)
}
