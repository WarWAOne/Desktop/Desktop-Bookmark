package main

import (
	"warwaone.com/Framework/JsonDatabase"
)

type BookmarkTable struct {
	JsonDatabase.Table
}

type Bookmark struct {
	Id       int    `json: "id"`
	Name     string `json: name`
	Url      string `json: url`
	Category string `json: category`
	Image    string `json: image`
}

/*func (table *BookmarkTable) getAll(w http.ResponseWriter, r *http.Request) {
	file, err := ioutil.ReadFile(table.Path)
	if err != nil {
		fmt.Println(err)
		w.Write([]byte("Error while reading json file: " + table.Path))
		return
	}
	//bookmarks := make([]Bookmark, 0)
	bookmarks := ""
	json.Unmarshal([]byte(string(file)), &bookmarks)
	w.Write([]byte(bookmarks))
}*/

/*func (table *BookmarkTable) get(w http.ResponseWriter, r *http.Request) {
	//ctx = r.Context()
	// TODO: see how to get the id from url
	id := chi.URLParam(r, "id")
	bookmarks := table.getAll()
	for _, bookmark := range bookmarks {
		if string(bookmark.Id) == id {
			//w.Write([]byte(bookmark)) // TODO: see how to send json bookmarks
		}
	}
}

func (table *BookmarkTable) create(bookmark Bookmark) {
	// TODO: Write json bookmark to array of json bookmarks
}

func (table *BookmarkTable) delete(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	bookmark := table.get(id)
	// TODO: remove json bookmark from the json array
}

func (table *BookmarkTable) edit(bookmark Bookmark) {
	oldBookmark := table.get(bookmark.Id)
	// TODO: replace it by the new one
}*/
