package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/user"

	"github.com/zserge/webview"
	"warwaone.com/Framework/JsonDatabase"
)

const APPS_DATA_FOLDER = "/WaoAppsData"
const APPS_FOLDER = APPS_DATA_FOLDER + "/api"
const DATABASE_FOLDER = APPS_DATA_FOLDER + "/database"
const LOCAL_API_FOLDER = "./API"

const TABLE_BOOKMARK = "bookmark"
const TABLE_CATEGORY = "category"

func main() {
	// Get user's home directory
	user, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	home := user.HomeDir

	// Create database folder if not exists
	db := JsonDatabase.Database{Path: home + "/" + DATABASE_FOLDER}

	path := LOCAL_API_FOLDER + "/readme.md"
	dest := home + APPS_DATA_FOLDER + "/readme.md"
	errReadme := CopyFile(path, dest)
	if errReadme != nil {
		fmt.Printf("CopyFile "+path+" to "+dest+" failed %q\n", errReadme)
	}

	// Create folders : /home/username/WaoAppsData/api
	os.Mkdir(home+"/"+APPS_FOLDER, 0700)

	// Copy API/bookmarkApi.go to API folder in /home/username/WaoAppsData/api
	path = LOCAL_API_FOLDER + "/bookmarkApi.go"
	dest = home + APPS_FOLDER + "/bookmarkApi.go"
	errBookmarkApi := CopyFile(path, dest)
	if errBookmarkApi != nil {
		fmt.Printf("CopyFile "+path+" to "+dest+" failed %q\n", errBookmarkApi)
	}

	// Copy API/bookmarkTable.go to API folder in /home/username/WaoAppsData/api
	path = LOCAL_API_FOLDER + "/bookmarkTable.go"
	dest = home + APPS_FOLDER + "/bookmarkTable.go"
	errBookmarkTable := CopyFile(path, dest)
	if errBookmarkTable != nil {
		fmt.Printf("CopyFile "+path+" to "+dest+" failed %q\n", errBookmarkTable)
	}

	// Create tables
	db.CreateTable(JsonDatabase.Table{Name: TABLE_BOOKMARK})
	db.CreateTable(JsonDatabase.Table{Name: TABLE_CATEGORY})

	// TODO: Start API

	webview.Open("Bookmarks",
		"http://localhost:3000/", 800, 600, true)
}

func CopyFile(src, dst string) (err error) {
	sfi, err := os.Stat(src)
	if err != nil {
		return
	}
	if !sfi.Mode().IsRegular() {
		// cannot copy non-regular files (e.g., directories,
		// symlinks, devices, etc.)
		return fmt.Errorf("CopyFile: non-regular source file %s (%q)", sfi.Name(), sfi.Mode().String())
	}
	dfi, err := os.Stat(dst)
	if err != nil {
		if !os.IsNotExist(err) {
			return
		}
	} else {
		if !(dfi.Mode().IsRegular()) {
			return fmt.Errorf("CopyFile: non-regular destination file %s (%q)", dfi.Name(), dfi.Mode().String())
		}
		if os.SameFile(sfi, dfi) {
			return
		}
	}
	if err = os.Link(src, dst); err == nil {
		return
	}
	err = copyFileContents(src, dst)
	return
}

func copyFileContents(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return
	}
	err = out.Sync()
	return
}
